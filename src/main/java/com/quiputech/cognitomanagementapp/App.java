package com.quiputech.cognitomanagementapp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.csvreader.CsvReader;

/**
 *
 * @author michael
 */
public class App {

    public static void main(String[] args) {
        if (args != null && args.length > 0) {
            try {

                List<Usuario> usuarios = new ArrayList<>();

                CsvReader usuarios_import = new CsvReader(args[0]);
                usuarios_import.readHeaders();

                while (usuarios_import.readRecord()) {
                    String nombreCompleto = usuarios_import.get("Usuario");
                    String espacio = " ";
                    String nombre = nombreCompleto.substring(0, nombreCompleto.indexOf(espacio));
                    String apellido = nombreCompleto.substring(nombreCompleto.indexOf(espacio) + 1, nombreCompleto.length());
                    String email = usuarios_import.get("Correo");
                    String directorio = usuarios_import.get("Directorio");
                    String perfil_1 = usuarios_import.get("Perfil_1");
                    String perfil_2 = usuarios_import.get("Perfil_2");

                    usuarios.add(new Usuario(nombre, apellido, email, directorio, perfil_1, perfil_2));
                }

                usuarios_import.close();

                for (Usuario us : usuarios) {

                    boolean created = CognitoManager.createCognitoAccount(us.getEmail(), us.getNombre(), us.getApellido());

                    if (!created) {
                        System.out.println(us.getEmail() + " Account not created");
                    } else {
                        System.out.println(us.getEmail() + " created");
                        //
                        if (!us.getPerfil_1().isEmpty()) {
                            CognitoManager.addUserToGroup(us.getEmail(), us.getPerfil_1());
                        }
                        if (!us.getPerfil_2().isEmpty()) {
                            CognitoManager.addUserToGroup(us.getEmail(), us.getPerfil_2());
                        }
                    }
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Ruta de archivo no valida.");
        }
    }
}
