package com.quiputech.cognitomanagementapp;

/**
 *
 * @author Michael Galdamez
 */
public class Usuario {
 
    private String nombre;
    private String apellido;
    private String email;
    private String directorio;
    private String perfil_1;
    private String perfil_2;
 
    public Usuario(String nombre, String apellido, String email, String directorio, String perfil_1,String perfil_2) {
        setNombre(nombre);
        setApellido(apellido);
        setEmail(email);
        setDirectorio(directorio);
        setPerfil_1(perfil_1);
        setPerfil_2(perfil_2);
    }
 
    public String getEmail() {
        if (email == null)
        email = "";
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
 
    public String getNombre() {
        if (nombre == null)
        nombre = "";
        return nombre;
    }
 
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
 
    public String getApellido() {
        if (apellido == null)
        apellido = "";
        return apellido;
    }
 
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
 
    public String getDirectorio() {
        if (directorio == null)
        directorio = "";
        return directorio;
    }
 
    public void setDirectorio(String directorio) {
        this.directorio = directorio;
    }
    
    public String getPerfil_1() {
        if (perfil_1 == null)
        perfil_1 = "";
        return perfil_1;
    }
 
    public void setPerfil_1(String perfil_1) {
        this.perfil_1 = perfil_1;
    }
    
    public String getPerfil_2() {
        if (perfil_2 == null)
        perfil_2 = "";
        return perfil_2;
    }
 
    public void setPerfil_2(String perfil_2) {
        this.perfil_2 = perfil_2;
    }
}
