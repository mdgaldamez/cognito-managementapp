import com.quiputech.cognitomanagementapp.CognitoManager;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author giancarlo
 */
public class CognitoManagerTest {
    
    public CognitoManagerTest() {
    }
    
    @Test(enabled = true)
    public void testUpdate() {
        
        //boolean result = CognitoManager.updateCognitoAccount("104", "kbazan@promotick.com", "Karla", "Bazan", true);
        //boolean result = CognitoManager.updateCognitoAccount("127", "rita.gutierrez@comercio.com.pe", "Rita", "Gutierrez", true);
        //boolean result = CognitoManager.updateCognitoAccount("92", "nevenka.vasquez@miraflores.gob.pe", "Nevenka", "Vasquez", true);
        //boolean result = CognitoManager.updateCognitoAccount("138", "mchira@motivasoluciones.com", "Maricely", "Chira", true);
        
        //boolean result = CognitoManager.updateCognitoAccount("37", "robinson.campos@bbva.com", "Robinson", "Campos", true);
        //boolean result = CognitoManager.updateCognitoAccount("37", "ac.sifuentes@bbva.com", "Alessandra", "Sifuentes", true);
        //boolean result = CognitoManager.updateCognitoAccount("117", "dianamendz@gmail.com", "Diana", "Mendoza", true);
        //boolean result = CognitoManager.updateCognitoAccount("127", "karina.soto@comercio.com.pe", "Karina", "Soto Rodriguez", true);
        //boolean result = CognitoManager.updateCognitoAccount("127", "rita.gutierrez@comercio.com.pe", "Rita", "Gutierrez Ramos", true);
        //boolean result = CognitoManager.updateCognitoAccount("153", "spercas@bancofalabella.com.pe", "S", "Percas", true);

        boolean result;

        //CognitoManager.updateCognitoAccount("86", "anusser@intercorp.com.pe", "S", "Amalia Nusser", true);
        //CognitoManager.updateCognitoAccount("86", "jvenegas@intercorp.com.pe", "S", "Jeremy Venegas", true);
        //result = CognitoManager.updateCognitoAccount("86", "apachecov@intercorp.com.pe", "S", "Augusto Pacheco", true);
        //result = CognitoManager.updateCognitoAccount("86", "jvenegas@intercorp.com.pe", "S", "Jeremy Venegas", true);

        result = CognitoManager.updateCognitoAccount("156", "gsoto@cajaarequipa.pe", "S", "Galea Soto", true);

        assertTrue(result);
        
    }

    
}
